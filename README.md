# font-recursive-nerdfont

The Recursive font patched with icons via Nerd Fonts.

Source fonts: https://github.com/arrowtype/recursive
Recursive web site: https://www.recursive.design/

# License

Recursive is licensed under the SIL Open Font License 1.1. This repo
does not represent a new font, simply the original source with Nerd
Icons attached.
